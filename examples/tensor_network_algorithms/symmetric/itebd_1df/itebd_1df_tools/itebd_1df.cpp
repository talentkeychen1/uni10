#include "itebd_1df_tools.h"
#include "itebd_1df.h"

//~/GitRepo/tensorlib/uni10/example/non-symmetry/return_linalg_funcs/itebd_1d/itebd_tools
template<typename T>
iTEBD_1D<T>::iTEBD_1D(const UniTensor<T>& _H,  const itebd_paras& paras, const map<string, Network*>& nets): H(_H), itebd_1df_nets(nets){

  fprintf(stdout, "\n");
  paras.print_info();
  fprintf(stdout, "\n");

  dim = H.bond(0).dim();
  D = paras.D;
  max_N   = paras.max_N;
  tau     = paras.tau;
  eps     = paras.eps;
  cut_off = paras.cut_off;
  measure_per_n_iter = paras.measure_per_n_iter;

  max_D = (uni10_int)cut_off == -1 ? D : paras.max_D;
  this->init();

}

template<typename T>
void iTEBD_1D<T>::init(){

  Bond bdi_mid = H.bond(0);
  Bond bdo_mid = H.bond(2);

  vector<Bond> gbonds;
  gbonds.push_back(H.bond(0));
  gbonds.push_back(H.bond(0));
  gbonds.push_back(H.bond(2));
  gammas.push_back(UniTensor<T>(gbonds, "GA"));
  gammas.push_back(UniTensor<T>(gbonds, "GB"));

  gammas[0].Randomize(); gammas[1].Randomize();

  vector<Bond> lbonds;
  lbonds.push_back(H.bond(0));
  lbonds.push_back(H.bond(2));
  lambdas.push_back(UniTensor<T>(lbonds, "LA"));
  lambdas.push_back(UniTensor<T>(lbonds, "LB"));

  lambdas[0].Randomize(); lambdas[1].Randomize();

}

template<typename T>
iTEBD_1D<T>::~iTEBD_1D(){


}

// Set hamiltonian in iTEBD algorithm.
template<typename T>
void iTEBD_1D<T>::setHamiltonian(const UniTensor<T>& _H){

  H = _H;

}

// Get the gate 
template<typename T>
UniTensor<T> iTEBD_1D<T>::get_gate(){

  UniTensor<T> U(H.bond());

  vector<Qnum> blk_qnums = H.BlocksQnum();
  for(vector<Qnum>::iterator it = blk_qnums.begin(); it != blk_qnums.end(); it++)
    U.PutBlock(*it, ExpH(-tau, H.GetBlock(*it)));

  return U;

}

template<typename T>
void iTEBD_1D<T>::Optimize(){

  double delta = 0.1;
  cout << H << endl;;

  UniTensor<T> U = this->get_gate();

  UniTensor<T> tmp, theta;

  for (uni10_int i=0; i < (uni10_int)max_N; i++){

    uni10_int A = i%2;
    uni10_int B = (i+1)%2;

    UniTensor<T> theta;
    ContractArgs(theta, *itebd_1df_nets["theta"], lambdas[B], gammas[A], lambdas[A], gammas[B], lambdas[B], U);

    theta.PrintDiagram();
    exit(0);


    if (i%measure_per_n_iter==0){

      UniTensor<T> ctheta;
      Conj(ctheta, theta, INPLACE);
      UniTensor<T> val = Contract(theta, ctheta);
      progressbar(i+measure_per_n_iter, 0, max_N);
      cout.precision(8);
      cout.setf(ios::fixed, ios::floatfield);
      cout <<  ", ge: " << -log(Real(val[0]))/delta/2.  << "\r";
      std::cout.flush();

    }

  }
  
  fprintf(stdout, "\n\n");

}

template class iTEBD_1D<uni10_double64>;
template class iTEBD_1D<uni10_complex128>;
