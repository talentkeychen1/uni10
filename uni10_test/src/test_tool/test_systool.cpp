#include <unistd.h>
#include <sys/syscall.h>

#include <string>
#include <fstream>

int getMaxRss()
{
  pid_t tid = syscall(SYS_gettid);

  std::ifstream status("/proc/self/task/" + std::to_string(tid) + "/status",
                       std::ios_base::in);

  std::string word;
  int         VmHWM;
  while (status >> word) {
    if (word == "VmHWM:") {
      status >> VmHWM;
      status.close();
      return VmHWM;
    }
  }

  return 0;
}
